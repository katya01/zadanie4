
package lmp.calculation;


import java.util.Arrays;

public class Algorithm {
    public static double theGreatestRoot(SquareTrinomial s) {
        double great;
        Arrays.sort(s.quadraticSolution());
        great = s.quadraticSolution()[1];
        return great;
    }

    public static void main(String[] args) {
        SquareTrinomial s1 = new SquareTrinomial(3, 6, 3);
        System.out.println(theGreatestRoot(s1));
    }
}
