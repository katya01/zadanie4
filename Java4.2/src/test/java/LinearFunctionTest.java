
import org.junit.Test;



import static org.junit.Assert.assertEquals;


public class LinearFunctionTest {
    @Test
    public void FunctionValue()
    {
        LinearFunction linearFunction = new LinearFunction(2.0,3.0,0.0,3.0);
        assertEquals(7.0,linearFunction.functionValue(2.0),1e-9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void FunctionValue1() {
       LinearFunction linearFunction1 = new LinearFunction(3.1,1.2,2.3,1.2);
        linearFunction1.functionValue(2);

    }

    @Test(expected = IllegalArgumentException.class)
    public void FunctionValue2() {
        LinearFunction linearFunction1 = new LinearFunction(3.1,1.2,1.2,3.2);
        linearFunction1.functionValue(5);
    }

}