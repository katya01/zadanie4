
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SinTest {

    @Test
    public void functionValue() {
        Sin sin = new Sin(1, 0, 0.0, 2.0);
        assertEquals(0.0, sin.functionValue(1), 1e-9);
    }
}