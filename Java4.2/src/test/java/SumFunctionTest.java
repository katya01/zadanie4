import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumFunctionTest {
    @Test
    public void testSumOfFunctionValuesExp(){
        Exp exp =new Exp(0,1,0,1);
        Exp exp1 =new Exp(1,2,3,4);
        SumOfFunctionValues s=new SumOfFunctionValues();
        assertEquals(3.0,s.functionality(exp),1e-9);
        assertEquals(113.79913891502422,s.functionality(exp1),1e-9);
    }
    @Test
    public void testSumOfFunctionValuesFraction(){
        SumOfFunctionValues s=new SumOfFunctionValues();
        FractionalEquation e=new FractionalEquation(1,2,3,5,0,1);
        FractionalEquation e1=new FractionalEquation(4,5,1,5,0,1);

        assertEquals(1.1596153846153847,s.functionality(e),1e-9);
        assertEquals(3.7727272727272725,s.functionality(e1),1e-9);

    }
    @Test
    public void testSumOfFunctionValuesLinear(){
        SumOfFunctionValues s=new SumOfFunctionValues();
       LinearFunction l=new LinearFunction(1,2,3,4);
        LinearFunction l1=new LinearFunction(3,6,3,4);
        assertEquals(16.5,s.functionality(l),1e-9);
        assertEquals(49.5,s.functionality(l1),1e-9);

    }
    @Test
    public void testSumOfFunctionValuesSin(){
        SumOfFunctionValues s=new SumOfFunctionValues();
    Sin sin=new Sin(1,2,4,6);
        Sin sin1=new Sin(3,6,2,8);
         assertEquals(-0.09123578226642293,s.functionality(sin),1e-9);
        assertEquals(-6.8785776102508915,s.functionality(sin1),1e-9);

    }
}
