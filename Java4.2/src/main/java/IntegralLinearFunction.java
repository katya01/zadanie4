
public class IntegralLinearFunction implements FunctionalityFromASingleArgument<LinearFunction> {


    public double functionality(LinearFunction linearFunction) {
        double integral = 0;
        int n = 100;
        double h = (linearFunction.getFinish() - linearFunction.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * linearFunction.functionValue(linearFunction.getStart() + i * h);
        }
        return integral;
    }
}
