
public interface FunctionalityFromASingleArgument<FunctionOnTheSegment> {
    double functionality(FunctionOnTheSegment one);

}
