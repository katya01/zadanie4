
public class SumOfFunctionValues implements FunctionalityFromASingleArgument<FunctionOnTheSegment> {

    @Override
    public double functionality(FunctionOnTheSegment one) {
        double sum;
        sum = one.functionValue(one.getStart());
        sum += one.functionValue(one.getFinish());
        sum += one.functionValue(one.getMiddle());
        return sum;
    }
}
