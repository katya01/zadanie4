
public class IntegralSin implements FunctionalityFromASingleArgument<Sin> {
    private Sin sin;

    public double functionality(Sin sin) {
        double integral = 0;
        int n = 100;
        double h = (sin.getFinish() - sin.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * sin.functionValue(sin.getStart() + i * h);
        }
        return integral;
    }

}
