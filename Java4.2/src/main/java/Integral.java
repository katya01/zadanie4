

public class Integral implements FunctionalityFromASingleArgument<FunctionOnTheSegment> {

    @Override
    public double functionality(FunctionOnTheSegment one) {
        double integral = 0;
        int n = 100;
        double h = (one.getFinish() - one.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * one.functionValue(one.getStart() + i * h);
        }
        return integral;
    }
}
