
public interface FunctionOnTheSegment {
    double functionValue(double x);

    double getStart();

    double getFinish();

    double getMiddle();
}
